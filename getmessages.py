from datetime import datetime
import requests
import json
from typing import NoReturn, List
import logging
import sys

logging.basicConfig(level=logging.INFO)


def get_commit_messages(account: str, repo: str) -> NoReturn:
    """This method attempts to grab all the commit messages from a given repository associated with a given account
    on BitBucket. On success, it will write the messages in chronological (i.e. by date) order to a file with the name
    pattern {account}_{repo}_commits.txt
    Parameters:
        account: str -> BitBucket account name
        repo: str -> Name of repository to grab commits from"""
    logging.debug(f"Attempting to access repo {repo} from account {account}.")
    url = f"https://api.bitbucket.org/2.0/repositories/{account}/{repo}/commits/?fields=values.message,values.date,next"
    all_commits = list()
    page = 1
    while url is not None:
        response = requests.get(url, timeout=5)
        if response.status_code != 200:
            logging.error(f"Couldn't access repo \"{repo}\" from account {account}")
            return
        else:
            logging.info(f"Repo \"{repo}\" from account {account} successfully accessed. Page {page}")
            response_json = json.loads(response.text)
            commits_json = response_json["values"]
            all_commits.extend(commits_json)
            url = response_json.get("next", None)
            page += 1
    logging.info("Done grabbing commits from the repo.")
    sorted_commits = _sort_commits(all_commits)
    sorted_messages = [commit["message"].rstrip() for commit in sorted_commits]
    _write_commits(sorted_messages,
                   f"{account}_{repo}_commits.txt"
                   )


def _sort_commits(commits: List[dict]) -> List[dict]:
    return sorted(commits, key=lambda commit: _extract_date(commit["date"]))


def _write_commits(commits: List[str], filename: str) -> NoReturn:
    logging.info(f"Writing commits to {filename}.")
    with open(filename, 'w', encoding='utf-8') as f:
        f.write("\n".join(commits))


def _extract_date(datetime_str: str) -> datetime:
    """Takes in a BitBucket-formatted date field (e.g. 2019-04-21T22:47:55+00:00) as a string and returns a datetime
    object representing that time. Because commits aren't necessarily in reverse-chronological order, this sorting is
    required."""
    return datetime.strptime(_rreplace(datetime_str, ":", "", 1), "%Y-%m-%dT%H:%M:%S%z")


def _rreplace(string: str, old: str, new: str, occurrences: int) -> str:
    """This method replaces a specified number of occurrences of a string sequence with another sequence, starting from
    the right of the string
    Parameters:
        string: str -> original string to be altered
        old: str -> sequence of characters in the string to be replaced
        new: str -> sequence of characters to replace occurrences of 'old' sequence
        occurrences: int -> number of occurrences to replace"""
    li = string.rsplit(old, occurrences)
    return new.join(li)


def main() -> NoReturn:
    args = sys.argv[1:]
    if len(args) != 2:
        logging.error("Incorrect number of arguments. The first argument should be an account name and the "
                      "second should be a repository name.")
    else:
        account, repo = args
        get_commit_messages(account, repo)


if __name__ == "__main__":
    main()
