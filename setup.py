from setuptools import setup

setup(
    name='api-test',
    version='0.1',
    modules=['getmessages'],
    author='Stephen Lowery',
    author_email='stephen.g.lowery@gmail.com',
    description='BitBucket commit-message-grabber',
    install_requires=['requests']
)
