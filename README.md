# BitBucket Commit Message Grabber
Given a valid BitBucket account name and repository, this program will grab all commit messages and write them to a file, sorted by date.

# Dependencies
Requires Python 3.5+ and the library `requests`.

# How to Use
- Clone files to a folder on your machine
- Open the command line and navigate to the folder
- Run `python setup.py install`
- Call the program with `getmessages.py [account name] [repository name]`
- The commits will be written to a .txt file with the pattern {account}_{repo}_commits.txt
